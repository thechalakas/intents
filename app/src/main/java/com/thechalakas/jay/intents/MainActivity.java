package com.thechalakas.jay.intents;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("MainActivity","onCreate - Reached");

        //grab the button

        Button button_Activity2 = (Button) findViewById(R.id.buttonGoToActivity2);

        //set listener
        button_Activity2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.i("MainActivity","button_Activity2 - setOnClickListener - onClick - reached");

                //create an intent to go the next activity
                //this is an implicit intent
                Intent intent = new Intent(getApplicationContext(),Main2Activity.class);
                startActivity(intent);
            }
        });

        Button button_Email = (Button) findViewById(R.id.buttonEmail);

        button_Email.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.i("MainActivity","button_Email - setOnClickListener - onClick - reached");

                //create a new intent for email
                //This would be an explicit intent as it provides user with multiple options
                Intent email = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:"));
                email.putExtra(Intent.EXTRA_TEXT,"Hello. This is the email text");
                startActivity(Intent.createChooser(email,"select your email app"));
            }
        });

        Button button_Dial = (Button) findViewById(R.id.buttonDial);

        button_Dial.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                Log.i("MainActivity","button_Dial - setOnClickListener - onClick - reached");

                //intent to launch dialer
                //this is an implicit intent
                Intent dialer = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:1234567890"));
                startActivity(dialer);

            }
        });

        Button button_Data = (Button) findViewById(R.id.buttonData);

        button_Data.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.i("MainActivity","button_Data - setOnClickListener - onClick - reached");

                Intent intent = new Intent(getApplicationContext(),Main3Activity.class);
                intent.putExtra("Data1","This is the first line of data");
                intent.putExtra("Data2","This is the second line of data");
                startActivity(intent);
            }
        });
    }//end of OnCreate


}
