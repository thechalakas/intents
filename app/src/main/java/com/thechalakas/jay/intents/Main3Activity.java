package com.thechalakas.jay.intents;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class Main3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        Log.i("Main3Activity","onCreate - Reached");

        //String s = getIntent().getStringExtra("EXTRA_SESSION_ID");
        String text1 = getIntent().getStringExtra("Data1");
        String text2 = getIntent().getStringExtra("Data2");

        Log.i("Main3Activity","Text 1 - " + text1 + "   Text 2 - "+text2);
    }
}
